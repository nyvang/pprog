#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int orbit_equation
(double t, const double y[], double dydt[], void * params)
{
   double epsilon = *(double *) params;
   dydt[0]=y[1];
   dydt[1]= 1 + epsilon * y[0] * y[0] - y[0];
   return GSL_SUCCESS;
}

int main(int argc, char *argv[]){
   double epsilon = 0, u_prime = 0; //  u_prime = 0 (u'(0))
   if(argc != 3) { //checking that there are 2 parameters as input
      fprintf(stderr,"Number of input parameters is wrong, stopping\n");
      exit (EXIT_FAILURE);
//    fprintf(stderr,"Number of input parameters is wrong,\n");
//    fprintf(stderr,"Usage should be: epsilon u_prime\n");
//    fprintf(stderr,"Using epsilon = 0 and u'(0) = 0.5\n");
//    epsilon = 0;
//    u_prime = 0.5;
   }
   else{
   epsilon = atof(argv[1]);
   u_prime = atof(argv[2]);
   }
   fprintf(stderr,"epsilon is = %g\n",epsilon);
   fprintf(stderr,"u_prime is = %g\n",u_prime);
   //  u_condition = atof(argv[2]);
 //  u_prime_condition = atof(argv[3]);
   gsl_odeiv2_system sys;
   sys.function = orbit_equation;
   sys.jacobian = NULL;
   sys.dimension = 2;

 //  double param = &epsilon;
   sys.params = (void*) &epsilon;
   double acc=1e-6,eps=1e-6,hstart=1e-3;
   gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new
      (&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);
 //  double t=0,y[2]={u_condition,u_prime_condition};
   double t=0,y[2]={1,u_prime};

   double theta_min=0,theta_max=160,delta_theta=0.1;
   for(double x=theta_min;x<theta_max;x+=delta_theta){
      gsl_odeiv2_driver_apply(driver,&t,x,y);  
      
      printf("%g %g\n",x,y[0]);
   }
   gsl_odeiv2_driver_free(driver);

   return 0;
}
