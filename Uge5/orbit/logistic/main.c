#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int differential_equation
(double t, const double y[], double dydt[], void * params)
{
   dydt[0]=y[0]*(1-y[0]);
   return GSL_SUCCESS;
}

double logistic_function(double x){
   gsl_odeiv2_system sys;
   sys.function = differential_equation;
   sys.jacobian = NULL;
   sys.dimension = 1;
   sys.params = NULL;
   double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
   gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new
      (&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);
   double t=0,y[1]={0.5};
   gsl_odeiv2_driver_apply(driver,&t,x,y);

   gsl_odeiv2_driver_free(driver);
   return y[0];
}

int main(){
   double a=0,b=3,dx=0.1;
   for(double x=a;x<=b;x+=dx)printf("%g %g\n",x,logistic_function(x));
   return 0;
}
