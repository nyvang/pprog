#include<stdio.h>
#include<math.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
double oscillator(double x){
   return 0.5*x*x;
}

double coulomb(double x){
   return -1/x;
}

int main(){
   int n = 650;
//  double s = 1.0/(n+1);
//   double osc_start = -10; double osc_end = 10;
   double coulomb_start = 0, coulomb_end = 37;
   double s = (coulomb_end - coulomb_start)/(n+1); //changed due to coulomb potential
//   double s = (osc_end - osc_start)/(n+1);
   gsl_matrix *H = gsl_matrix_calloc (n,n);
   for(int i = 0; i<n-1;i++){ //Set elements of matrix
      gsl_matrix_set (H,i,i,-2);
      gsl_matrix_set (H,i,i+1,1);
      gsl_matrix_set (H,i+1,i,1);
   }
   gsl_matrix_set (H,n-1,n-1,-2);
   gsl_matrix_scale (H,-0.5/s/s);

   gsl_vector *x = gsl_vector_alloc(n);
   for(int i = 0; i<n; i++){
    //  gsl_vector_set(x,i,osc_start+(i+1)*s);
      gsl_vector_set(x,i,coulomb_start+(i+1)*s);
   }
   for(int i = 0; i < n; i++){
      double Hii = gsl_matrix_get(H,i,i);
      double xi = gsl_vector_get(x,i);
   //   double Poti = oscillator(xi);
      double Coi = coulomb(xi);
      gsl_matrix_set(H,i,i,Hii+Coi);
   }
   gsl_eigen_symmv_workspace* w = gsl_eigen_symmv_alloc (n); //workspace vector w
   gsl_vector* eval = gsl_vector_alloc(n);
   gsl_matrix* evec = gsl_matrix_calloc(n,n);
   gsl_eigen_symmv(H,eval,evec,w); //compute eigenvalues and eigenvectors of H
// and store in eval and evec   
   gsl_eigen_symmv_sort(eval,evec,GSL_EIGEN_SORT_VAL_ASC);
   fprintf (stderr, "i   exact   calculated\n");
   for (int k=0; k < n/3; k++){
   double exact = -1/2.0 * 1.0/(k+1)/(k+1);
//   double exact = k+1/2.0;
//    double exact = pow(M_PI,2)*pow(k+1,2);
   double calculated = gsl_vector_get(eval,k);
   fprintf (stderr, "%i   %g   %g\n", k, exact, calculated);
   }
   for(int k=0;k<3;k++){
   printf("%g %g\n",coulomb_start,0.0);
      for(int i=0;i<n;i++) printf("%g %g\n",gsl_vector_get(x,i),gsl_matrix_get(evec,i,k));
//   for(int i=0;i<n;i++) printf("%g %g\n",(i+1.0)/(n+1),gsl_matrix_get(evec,i,k));
   printf("%g %g\n",coulomb_end,0.0);
   printf("\n\n");
   }
   gsl_vector_free(x);
   gsl_matrix_free(evec);
   gsl_vector_free(eval);
   gsl_matrix_free(H);
   gsl_eigen_symmv_free(w);
   return 0;
}
