#include<stdio.h>
#include<gsl/gsl_linalg.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

int main(){
   double a_data[] = {
   6.13, -2.90, 5.86,
   8.08, -6.31, -3.89,
   -4.36, 1.00, 0.19 };
   double b_data[] = {6.23,5.37,2.29};
   printf("Solving set of linear equations ax = b with LU decomposition using a = \n");
      for(int row=0; row<3;row++){
         for(int col=0; col<9; col+=3){
         printf("%g     ", a_data[col+row]);
         }
      printf("\n");
}
   printf("and b = \n");
   for (int columns=0; columns<3; columns++){
      printf("%g    ", b_data[columns]);
      printf("\n");
   }
   gsl_matrix_view m = gsl_matrix_view_array (a_data,3,3);
   gsl_vector_view b = gsl_vector_view_array (b_data,3);
   gsl_vector *x = gsl_vector_alloc(3);
   int s;
   gsl_permutation *p = gsl_permutation_alloc(3);
   gsl_linalg_LU_decomp (&m.matrix,p,&s);
   gsl_linalg_LU_solve (&m.matrix,p,&b.vector,x);
 //  gsl_linalg_HH_solve (&m.matrix,&b.vector,x);
   printf("x = \n"); 
   gsl_vector_fprintf (stdout,x,"%g");
   gsl_permutation_free(p);
   gsl_vector_free(x);
   return 0;
}
