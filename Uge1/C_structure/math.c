#include <stdio.h>
#include <tgmath.h>

int main(){
      double x=5;
      double gx = tgamma(x);
      double y=0.5;
      double by=j0(0.5);
      complex squ = csqrt(-2.0);
      complex ei = cexp(I);
      complex expi = cexp(I*M_PI);
      complex ipow = pow(I,exp(1.0));
      float numf = 0.1111111111111111111111111111;
      double numd= 0.1111111111111111111111111111;
      long double numl = 0.1111111111111111111111111111L;
      printf("gamma(%g)=%g\n",x,gx);
      printf("j0(%g)=%g\n",y,by);
      printf("sqrt(-2)=%g+i*%g\n",creal(squ),cimag(squ));
      printf("e^(i) =%g+i*%g\n",creal(ei),cimag(ei));
      printf("e^(pi*i) = %g + i*%g\n",creal(expi),cimag(expi));
      printf("i^e = %g + i*%g\n",creal(ipow),cimag(ipow));
      printf("0.1111111111111111111111111111 is with float precision = %.25g\n",numf);
      printf("0.1111111111111111111111111111 is with double precision = %.25g\n",numd);
      printf("0.1111111111111111111111111111 is with long double precision = %.25Lg\n",numl);
      
      return 0;
}
