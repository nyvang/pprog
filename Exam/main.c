#include<assert.h>
#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_sf_pow_int.h>
#include<math.h>
int diff_equation (double x, const double y[], double dydx[], void * params)
{
   dydx[0]=1/(2*y[0]);
   return GSL_SUCCESS;
}

double my_sqrt(double x)
{
   assert(x>=0);
//   double scalar = 1;
   if(x==0) return 0;
   if(x<1) return 1/my_sqrt(1.0/x);
   if(x>4)
   { 
      double div_4 = 0; 
      // div_4 is the number of times x can be divided by 4
      // before x os below 4 such that sqrt(x)=sqrt(x/(4^(div_4) ))*2^(div_4)
      while(x>4)
      { 
         x/=4;
         div_4+=1;
      }
      return gsl_sf_pow_int(2,div_4)*my_sqrt(x);
   }
   gsl_odeiv2_system sys;
   sys.function = diff_equation;
   sys.jacobian = NULL;
   sys.dimension = 1;
   sys.params = NULL;
   double acc=1e-6,eps=1e-6,hstart=1e-3;
   gsl_odeiv2_driver *driver = 
      gsl_odeiv2_driver_alloc_y_new(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);
   double t=1,y[1]={1};
   gsl_odeiv2_driver_apply(driver,&t,x,y);
   gsl_odeiv2_driver_free(driver);
   return y[0];
}

int main(int argc, char *argv[])
{
   double a = 0,b = 0, delta_x=0;
   if(argc ==4)
   {
   a=atof(argv[1]);
   b=atof(argv[2]);
   delta_x=atof(argv[3]);
   if(a < 0 || b < 0 || delta_x < 0 || abs(delta_x) > abs(a-b))
      {
         fprintf(stderr,"One or more of the input values are negative, or magnitude of incrementor value is too large, stopping\n");
         exit (EXIT_FAILURE);
      }
   printf("x    calculated    exact\n");   
   for(double x = a;x<b;x+=delta_x)
      {
      printf("%g    %g    %g\n",x,my_sqrt(x),sqrt(x) );
      }
   }
   else
   {
   fprintf(stderr,"Something went wrong here.\n Did you remember to make your input as: a b d \n where a is the lower bound, b is the upper bound and d the difference between x values?\n");
   exit (EXIT_FAILURE); 
   } 
   return 0;
}
