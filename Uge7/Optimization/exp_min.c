//Most of the outcommented lines are from a non-derivative based version

#include<stdio.h>
#include<math.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_vector.h>
#define f(t) A*exp(-(t)/T)+B //function that data will be fitted to.
//#define ALG gsl_multimin_fminimizer_nmsimplex2
#define ALGDER gsl_multimin_fdfminimizer_conjugate_fr //with derivatives

struct experimental_data {int n; double *t,*y,*e;};

double function_to_minimize (const gsl_vector *x, void *params) {
   double A = gsl_vector_get(x,0);
   double T = gsl_vector_get(x,1);
   double B = gsl_vector_get(x,2);
   struct experimental_data *p = (struct experimental_data*) params;
   int n = p->n;
   double *t = p->t;
   double *y = p->y;
   double *e = p->e;
   double sum = 0;
   for(int i=0;i<n;i++) sum+= pow(( f(t[i]) - y[i])/e[i],2);
   return sum;

}

void gradient (const gsl_vector *x, void *params, gsl_vector *df)
{
   double A = gsl_vector_get(x,0);
   double T = gsl_vector_get(x,1);
   double B = gsl_vector_get(x,2);
   struct experimental_data *p = (struct experimental_data*) params;
   int n = p->n;
   double *t = p->t;
   double *y = p->y;
   double *e = p->e;
   double sum_dev_A = 0; //derivative of function_to_minimize wrt A
   double sum_dev_T = 0; //derivative of function_to_minimize wrt T
   double sum_dev_B = 0; //derivative of function_to_minimize wrt B
   for(int i=0;i<n;i++){
      sum_dev_A+= 2.0*exp(-t[i]/T)*( f(t[i]) - y[i])/e[i];
      sum_dev_T+= 2.0*( f(t[i]) - y[i])/e[i];
      sum_dev_B+= 2.0*A*t[i]*exp(-t[i]/T)*( f(t[i]) - y[i])/(e[i]*pow(T,2));
   }
   gsl_vector_set(df,0,sum_dev_A);
   gsl_vector_set(df,1,sum_dev_T);
   gsl_vector_set(df,2,sum_dev_B);
}

void func_gradient(const gsl_vector *x,void *params, double *f, gsl_vector *df)
//Calculates function_to_minimize and gradient together
{
   *f = function_to_minimize (x,params);
   gradient(x,params,df);
}

int main(){
   double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
   double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
   double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
   int n = sizeof(t)/sizeof(t[0]); // number of data points
   size_t dim = 3; //3 parameters A, T and B
   struct experimental_data params;
   params.n = n;
   params.t = t;
   params.y = y;
   params.e = e;
   gsl_multimin_function_fdf F;
   //gsl_multimin_function F;
   F.f=function_to_minimize;
   F.n=dim;
   F.df=gradient;
   F.fdf = func_gradient;
   F.params=(void*)&params;
   
   gsl_multimin_fdfminimizer * state = gsl_multimin_fdfminimizer_alloc (ALGDER,dim);
   //gsl_multimin_fminimizer * state = gsl_multimin_fminimizer_alloc (ALG,dim);
   gsl_vector *start = gsl_vector_alloc(dim);
 //  gsl_vector *step = gsl_vector_alloc(dim);
   gsl_vector_set(start,0,4); // Initial A
   gsl_vector_set(start,1,5.1); // Initial T
   gsl_vector_set(start,2,1.3); // Initial B
 //  gsl_vector_set_all(step,0.01);
   //gsl_multimin_fminimizer_set (state,&F,start,step);


   gsl_multimin_fdfminimizer_set (state,&F,start,0.01,1e-4);

 
   int iter=0,status;
   double acc=0.001;

   do{
      iter++;
      int flag = gsl_multimin_fdfminimizer_iterate (state);
      //int flag = gsl_multimin_fminimizer_iterate (state);

      if(flag!=0)break;
      status = gsl_multimin_test_gradient (state->gradient, acc);
   //   status = gsl_multimin_test_size (state->size,acc);
      if(flag==GSL_SUCCESS) fprintf(stderr,"Convergence\n");
 
      fprintf(stderr,
     //       "iter=%2i, A = %8f, T = %8f, B = %8f, R = %8g, size = %8g\n",
            "iter=%2i, A = %8f, T = %8f, B = %8f, f = %8g\n",
            iter,
            gsl_vector_get(state->x,0),
            gsl_vector_get(state->x,1),
            gsl_vector_get(state->x,2),
      //      state->fval,
       //     state->size);
            state->f);
   }while(status == GSL_CONTINUE && iter < 99);
   double A = gsl_vector_get(state->x,0);
   double T = gsl_vector_get(state->x,1);
   double B = gsl_vector_get(state->x,2);
   printf("t    yexp    ycalc    err\n");
   for(int i=0;i<n;i++){
      printf("%g    %g    %g    %g\n",t[i],y[i],f(t[i]),e[i]);

   }

   gsl_vector_free(start);
 //  gsl_vector_free(step);
   gsl_multimin_fdfminimizer_free(state);
  // gsl_multimin_fminimizer_free(state);
   return 0;
}
