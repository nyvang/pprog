#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include"nvector.h"
#define RND (double)rand()/RAND_MAX

int main(){
   int n = 5;
   printf("Testing nvector_alloc\n");
   nvector *v = nvector_alloc(n);
   if(v == NULL) printf("Allocation not working\n");
   else printf("SUCCESFUL allocation!\n");
   
   printf("Testing nvector_set\n");
   nvector *w = nvector_alloc(n);
   double value = RND;
   int i = n/2;
   nvector_set(w,i,value);
   printf("w_%i = %g\n",i,value); 
   printf("Testing nvector_get\n");
   double get_test = nvector_get(w,i);
   printf("get_test = %g\n",get_test);
   printf("Testing nvector_dot_product\n");
   nvector *a = nvector_alloc(n);
   nvector *b = nvector_alloc(n);
   nvector *c = nvector_alloc(n);
   double actual_dot_product = 0;
   for(int k = 0; k<n; k++){
      double x = RND; double y = RND; double z = RND;
      nvector_set(a,k,x); 
      nvector_set(b,k,y);
      nvector_set(c,k,z);
      actual_dot_product += nvector_get(a,k) * nvector_get(b,k);
   }
   nvector_print("a = ",a);
   nvector_print("b = ",b);
   nvector_print("c = ",c);
   nvector *aa = nvector_alloc(n);
   for(i=0; i<a->size; i++){
      nvector_set(aa,i,a->data[i]);
   } 
   nvector_print("Duplicate vector aa = a is now set, so\naa =",aa);
   double dot_test = nvector_dot_product(a,b);
   printf("The dot product a*b is %g while nvector_dot_product gives %g\n",actual_dot_product,dot_test);
   printf("Testing scaling of vectors\n");
   double r = RND;
   printf("Random scalar is r = %g\n",r);
   nvector_scale(c,r);
   nvector_print("Now c =",c);
   printf("Testing nvector_equal with a and aa\n");
   if(nvector_equal(a,aa) == 1)printf("The vectors are equal!\n");
   else printf("The vectors are NOT equal!\n"); 
   nvector_free(v);
   nvector_free(w);
   nvector_free(a);
   nvector_free(aa);
   nvector_free(b);
   nvector_free(c);
   printf("Vectors are freed\n");

   return 0;
}
