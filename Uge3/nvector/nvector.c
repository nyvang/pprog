#include<stdio.h>
#include"nvector.h"
#include<stdlib.h>
#include<assert.h>
#include<tgmath.h>
nvector* nvector_alloc(int n){
   nvector* v = malloc(sizeof(nvector));
   (*v).size = n;
   (*v).data = malloc(n*sizeof(double));
   if( v==NULL ) fprintf(stderr,"error in nvector_alloc\n");
   return v;
}

void nvector_free (nvector* v){
   free(v);
   v = NULL;
}

void nvector_set (nvector* v, int i, double value){ //set value of v_i
   (*v).data[i] = value;

}

double nvector_get (nvector* v, int i){ //get value of v_i
   double value = (*v).data[i];
   return value;

}

double nvector_dot_product (nvector* u, nvector* v){
   double dot_product = 0;
   int i;
   assert( (*u).size == (*v).size );
   for (i = 0; i < (*v).size; i++){
         dot_product += nvector_get(u,i) * nvector_get(v,i);

         }
         return dot_product;
}

void nvector_print(char *s, nvector * v)
{
      printf("%s", s);
      for (int i = 0; i < v->size; i++)
            printf("%g ", v->data[i]);
      printf("\n");
}

int scalar_equal(double a, double b){
   if(fabs(a-b) < 1e-6) return 1;
   else return 0;
}

int nvector_equal(nvector *a,nvector *b){
   assert( a->size == b->size);
   int sum = 0;
   int size = a->size;
   for(int i=0;i<size;i++){
      sum+= scalar_equal(a->data[i],b->data[i]);
   }
   if(sum == a->size) return 1;
   else return 0;
}

void nvector_scale(nvector *v, double x){
   int i;
   for(i=0; i< v->size; i++){
      v->data[i] *= x;
   }
}
