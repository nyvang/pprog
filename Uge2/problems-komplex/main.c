#include<stdio.h>
#include<complex.h>
#include<tgmath.h>
#include"komplex.h"
#ifndef TINY
#define TINY 1e-6
#endif
int main(){
   komplex z;
   z.re=1;
   z.im=3;
   komplex p = {3,12};
   double x=3;
   double y=10;
   komplex_print("Before komplex_set z =",z);
   double set_a = 2.0; double set_b = 5.0;
   komplex_set (&z, set_a, set_b);
   komplex_print("Testing komplex_set: now z = ",z);
   printf("z should be = (%g,%g)\n",set_a,set_b);
   komplex k = komplex_new (x,y);
   komplex_print("Testing komplex_new: k is",k);
   printf("k should be (x,y)=(%g,%g)\n",x,y);
   printf("Now testing komplex_add\n");
   komplex_print("z is =",z);
   komplex_print("p is =",p);
   komplex add = komplex_add (z,p);
   komplex_print("z + p =",add);
   komplex ADDE = {5,17};
   komplex_print("It is actually z + p =",ADDE); 
   komplex diff = komplex_sub(add,ADDE);
   printf("Testing diff: add - ADDE = %g + i * %g\n", diff.re,diff.im);  
   if( komplex_equal(add,ADDE) ) printf("add and ADD are equal with a precision of 1e-6\n");
   else printf("Error: add and ADD are not equal\n");
   komplex k_conjugate = komplex_conjugate(k);
   komplex_print("Testing komplex conjugate for k =",k);
   komplex_print("conjugate gives conjugate(k) = ",k_conjugate);  
   printf("conjugate(k) should be = (%g,%g)\n",k.re,-k.im);
   printf("Now testing komplex_mul using k = (%g,%g) and p = (%g,%g)\n",k.re,k.im,p.re,p.im);
   komplex k_p_mul = komplex_mul(k,p);
   komplex_print("k*p = ",k_p_mul);
   printf("k*p should be (%g,%g)\n",k.re*p.re-k.im*p.im,k.re*p.im+k.im*p.re);
   return 0;
   
}
