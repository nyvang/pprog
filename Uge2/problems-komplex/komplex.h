#ifndef HAVE_KOMPLEX_H
struct komplex {double re,im;};
typedef struct komplex komplex;

void komplex_set (komplex * z, double x, double y); // set z = x + i*y
void komplex_print (char* s, komplex z);
komplex komplex_new (double x, double y); //change x to y
komplex komplex_add (komplex a, komplex b); // make new komplex number c = a + b
komplex komplex_sub (komplex a, komplex b); // c = a - b
komplex komplex_conjugate (komplex a); // set a + ib to a - ib
komplex komplex_mul (komplex a, komplex b); // a*b
int komplex_equal (komplex a, komplex b); // check if a = b
#define HAVE_KOMPLEX_H
#endif
