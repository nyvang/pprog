#include<stdio.h>
#include<tgmath.h>
#include<float.h>
#include"komplex.h"
#ifndef TINY
#define TINY 1e-6
#endif
//struct komplex {double re, im;};
//const struct komplex I ={0,1};
//typedef struct komplex komplex;

void komplex_print (char *s, komplex z) {
   printf("%s (%g, %g)", s, z.re, z.im);
   printf("\n");
}

void komplex_set (komplex * z, double x, double y){
   (*z).re = x;
   (*z).im = y;

}

komplex komplex_new (double x, double y){
   komplex number = {.re = x, .im = y};
   return number;
}

komplex komplex_add (komplex a, komplex b){
   komplex number = {a.re + b.re, a.im + b.im};
   return number;
}

komplex komplex_sub (komplex a, komplex b){
   komplex number = {a.re - b.re, a.im - b.im};
   return number;
}

komplex komplex_conjugate (komplex a){
   komplex number = {a.re, -a.im};
   return number;
}

komplex komplex_mul (komplex a, komplex b){
   komplex number = {a.re * b.re - a.im * b.im, a.re * b.im + a.im * b.re};
   return number;
}

int komplex_equal (komplex a, komplex b){
   if(fabs(a.re-b.re) < TINY && fabs(a.im-b.im) < TINY){return 1;}
   else return 0;   
}
