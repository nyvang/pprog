#ifndef HAVE_KOMPLEX_H

struct komplex {double re; double im;};
typedef struct komplex komplex;

komplex komplex_add      (komplex a, komplex b); /* returns a+b */
komplex komplex_sub      (komplex a, komplex b); /* returns a-b */
komplex komplex_mul      (komplex a, komplex b); /* returns a*b */
komplex komplex_div      (komplex a, komplex b); /* returns a/b */
komplex komplex_new      (double x, double y);   /* returns x+i*y */
void    komplex_set      (komplex* z, double x, double y);   /* z = x+i*y */
komplex komplex_conjugate(komplex z);            /* returns complex conjugate */
komplex komplex_exp      (komplex z);            /* returns complex exponential function of the argument */
void    komplex_print    (char* s, komplex z);   /* prints the complex number */
int     komplex_equal    (komplex a, komplex b); /* returns 1 if equal, 0 otherwise */

#define HAVE_KOMPLEX_H
#endif
