#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<tgmath.h>
int main(){
   int i = 1;
   int j;
   int k = 1;
   while(i+1>i) {i++;}
   printf("Using a while loop, my maximum integer is %i\n",i);
   printf("INT_MAX is %i\n",INT_MAX);
   for(j=1; j < j+1; j++);
      printf("Using a for loop, the maximum integer is %i\n",j);
   do {
      k++;
   }  while(k+1>k);
   printf("Using a do-while loop, the maximum integer is %i\n",k);
   int l=-1; int m; int n=-1;
   while(l-1<l) {l--;}
   printf("My calculated minimum integer is %i\n",l);
   printf("INT_MIN is %i\n",INT_MIN);
   for(m=-1; m-1 < m; m--);
   printf("Minimum integer using a for loop is %i\n",m);
   do {
      n--;
      }  while(n>n-1);
   printf("Minimum integer using a do-while loop is %i\n",n); 
   printf("Now for the machine epsilon test\n\n");
   float x=1.0f; while(1+x!=1){x/=2;} x*=2;
   printf("Using while loop, FLT_EPSILON is %g\n",x);
   float y; for(y=1.0f; 1+y!=1;y/=2){}; y*=2;
   printf("Using for loop, FLT_EPSILON is %g\n",y);
   float z=1.0f; do{
         z/=2;
         }while(1+z!=1); z*=2;
   printf("Using do-while loop, FLT_EPSILON is %g\n",z);
   printf("In limits.h, FLT_EPSILON is %g\n",FLT_EPSILON);
   double a=1; while(1+a!=1){a/=2;} a*=2;
   printf("Dbl epsilon from calculation is %g\n",a);
   printf("In limits.h, DBL_EPSILON is %g\n",DBL_EPSILON);
   long double b=1L; while(1+b!=1){b/=2;} b*=2;
   printf("Ldbl epsilon from calculation is %Lg\n",b);
   printf("In limits.h, LDBL_EPSILON is %Lg \n",LDBL_EPSILON);
   return 0;
   }
