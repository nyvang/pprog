#include<stdio.h>
#include<limits.h>
#include<float.h>
#include<tgmath.h>
#include<stdlib.h>
#define RND (double)rand()/(RAND_MAX*10.0)
int equal (double a, double b, double tau, double epsilon);

int main(){
   printf("INT_MAX is %i\n",INT_MAX);
   int max=INT_MAX/2;
   int i;
   float sum_up_float = 0.0f; //sum of i=1 to max for 1.0f/i
   float sum_down_float = 0.0f; // sum of i=max to 1 for 1.of/i
   printf("Float vs double sums coming\n");
   printf("max is %i; making sum(1/i) for i=1 to max in two ways\n",max);
   for(i=1; i<=max; i++){
   sum_up_float = sum_up_float + 1.0f/i;
   }
   printf("sum_up_float is %g\n",sum_up_float);
   for(i=max; 1<=i; i--){
   sum_down_float = sum_down_float + 1.0f/i;
   }
   printf("sum_down_float is %g\n",sum_down_float);
   printf("sum_down_float is larger than sum_up_float most likely due to roundoff errors making the numbers 'larger'\n");
   printf("smallest element is, in float, %f\n",1.0f/max);
   printf("smallest element is, in double, %g\n",1.0/max);
   printf("Down to max=INT_MAX/16, the numbers seem to be the same (not shown)\n");
   printf("Mathematically, the sum (i=1,infinity) 1/n is divergent; has checked with max up to INT_MAX*(1-1/3). Gives a higher sum than e.g. max=INT_MAX/2 \n");
   printf("Now comes double sums\n");
   double sum_up_double = 0, sum_down_double = 0;
   for(i=1; i<=max; i++){
   sum_up_double = sum_up_double + 1.0/i;
   }
   for(i=max; 1<=i; i--){
   sum_down_double = sum_down_double + 1.0/i;
   }
   printf("sum_up_double is %g\n",sum_up_double);
   printf("sum_down_double is %g\n",sum_down_double);
   printf("Double precision is precise enough to make the sums equal\n\n");
   
   printf("Now for the equality checking section\n");
   double tau = 1e-6; double epsilon = 1e-6;
   printf("Using tau and epsilon hardcoded as 1e-6, checking with random numbers \n");
   double a = RND; double b = RND;
   printf("a = %g and b = %g and abs(a-b) = %g\n",a,b,fabs(a-b));
   if( equal(a, b, tau, epsilon) == 1) printf("a and b are numerically equal\n");
   else printf("a and b are NOT equal\n");
   return 0;
}
