#include<math.h>
#include<stdio.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
int error_diff_equation(double x, const double y[], double dydt[], void* params){
   dydt[0]=(2.0/sqrt(M_PI))*exp(-x*x);
   return GSL_SUCCESS;
}

double error_function(double x){
   gsl_odeiv2_system sys;
   sys.function = error_diff_equation;
   sys.jacobian = NULL;
   sys.dimension = 1;
   sys.params = NULL;
   double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);
   gsl_odeiv2_driver *driver = gsl_odeiv2_driver_alloc_y_new
      (&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps);
   double t=0,y[1]={0.0};
   gsl_odeiv2_driver_apply(driver,&t,x,y);
   gsl_odeiv2_driver_free(driver);
   return y[0];
}

int main(int argc, char *argv[]){
   double a = 0, b = 0, dx = 0;
   if(argc<=1 || 5<=argc){
      printf("Number of arguments is incorrect; using a = 0, b = 3, dx=0.2\n");
      b = 3.0;
      dx = 0.2;
   }
   a = atof(argv[1]);
   b = atof(argv[2]);
   dx = atof(argv[3]);
//   double a=0,b=5,dx=0.1;
   for(double x=a;x<=b;x+=dx)printf("%g %g\n",x,error_function(x));
   return 0;
}
