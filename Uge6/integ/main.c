#include<stdio.h>
#include<math.h>
double energy(double);

int main(){
   double a=0.05, b=2.5, interval=0.01;
   printf("alpha    calculated\n");
   for(double alpha = a; alpha<b;alpha+=interval)
      printf("%g %g\n",alpha,energy(alpha));
   return 0;

}
