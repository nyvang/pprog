#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double ln_sq_integrand(double x, void* params){
   return log(x)/sqrt(x);
}

double analytical_integral(double a, double b){
   if(abs(a)<=1e-6){
   if(abs(a)<=1e-6 && abs(b)<=1e-6) return 0;
   else return 2*sqrt(b)*(log(b)-2);
         }
   if(abs(b)<=1e-6){
   return -2*sqrt(a)*(log(a)-2);
   }
   else return 2*sqrt(b)*(log(b)-2)-2*sqrt(a)*(log(a)-2);
}

double integral(double a, double b){
   gsl_function f;
   f.function = &ln_sq_integrand;
   f.params = NULL;

   int limit = 100;
   int key = 3;
   double acc=1e-6,eps=1e-6,result,err;
   gsl_integration_workspace * workspace = gsl_integration_workspace_alloc(limit);
   int status = 
      gsl_integration_qag(&f,a,b,acc,eps,limit,key,workspace,&result,&err);
   gsl_integration_workspace_free(workspace);
   if(status!=GSL_SUCCESS) return NAN;
   else return result;
}

int main(){
   double a=0, b=1;
   printf("a = %g, b = %g, integral = %g\n",a,b,integral(a,b));
   printf("The analytical result is: %g\n",analytical_integral(a,b)); 
   return 0;

}
