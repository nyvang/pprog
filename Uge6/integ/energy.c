#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double norm_integrand(double x, void* params){
   double z = *(double*)params;   
   return exp(-z*x*x);
}

double Hamiltonian_integrand(double x, void* params){
   double z = *(double*)params;
 //  return (-z*z*x*x/2 + z/2 + x*x/2)*exp(-z*x*x);
   return (-z*z*x*x + z + x*x)*exp(-z*x*x)/2;
}

double energy(double z){
   gsl_function f;
   f.function = norm_integrand;
   f.params = (void*)&z;

   int limit = 1000;
   //int key = 3;
   double acc=1e-7,eps=1e-7,result,err;
   gsl_integration_workspace * workspace = gsl_integration_workspace_alloc(limit);
   int status = gsl_integration_qagi(&f,acc,eps,limit,workspace,&result,&err);
   gsl_integration_workspace_free(workspace);
   if(status!=GSL_SUCCESS) return NAN;
  else{ double denominator = result;
  gsl_integration_workspace * workspace = gsl_integration_workspace_alloc(limit);
  f.function = Hamiltonian_integrand;
  status = gsl_integration_qagi(&f,acc,eps,limit,workspace,&result,&err);
  gsl_integration_workspace_free(workspace);
  if(status!=GSL_SUCCESS) return NAN;
   else return result/denominator;
  }
  }
